package com.example.bluethoohtutorial;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.UUID;

public class BluetoothConnectionService {
    private static final String TAG = "BluetoothConnectionServ";

    private static final String appName = "MYAPP";

    private static final UUID MY_UUID_INSECURE = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");

    private final BluetoothAdapter mBluetoothAdapter;
    Context mContext;

    private AcceptThread mInsecureAccpetThread;

    private ConnectThread mConnectThread;

    private BluetoothDevice mDevice;

    private UUID deviceUUID;

    ProgressDialog mProgressDialog;

    private ConnectedThread mConnectedThread;

    public BluetoothConnectionService(Context context){

        mContext = context;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        start();
    }


    private class AcceptThread extends  Thread {
        private final BluetoothServerSocket mServerSocket;

        public  AcceptThread(){
            BluetoothServerSocket tap = null;
            try{
                tap = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(appName, MY_UUID_INSECURE);
                Log.d(TAG,"AcceptThread: Setting up Server Using: " + MY_UUID_INSECURE);

            }catch (IOException e){
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage());

            }

            mServerSocket = tap;
        }
        public void run(){
            Log.d(TAG, "run: AcceptThread Running. ");
            BluetoothSocket socket = null;
            try{
                Log.d(TAG, "run: RFCOM server socket start......");

                socket = mServerSocket.accept();

                Log.d(TAG, "run: RFCOM server socket accepted connection");
            }catch (IOException e){
                Log.e(TAG, "AcceptThread: IOException: " + e.getMessage());
            }

            if(socket != null){
                connected(socket,mDevice);
            }

            Log.i(TAG, "END mAccpetThread ");
        }
        public  void cancel(){
            Log.d(TAG, "cancek: Canelling AcceptThread.");
            try{
                mServerSocket.close();
            }catch (IOException e){
                Log.e(TAG, "cancel: Close of AcceptThread ServerSocket failed. " + e.getMessage());
            }
        }
    }

    private class ConnectThread extends Thread{
        private BluetoothSocket mSocket;

        public  ConnectThread (BluetoothDevice device, UUID uuid){
            Log.d(TAG, "ConnectThread: started. ");
            mDevice = device;
            deviceUUID = uuid;
        }
        public void  run(){
            BluetoothSocket tap = null;
            Log.i(TAG, "RUN mConnectThread ");

            try{
                Log.d(TAG, "ConnectThread: Trying to create InsecureRFcomSocket using UUID:" + MY_UUID_INSECURE);
                tap = mDevice.createRfcommSocketToServiceRecord(deviceUUID);
            }catch (IOException e){
                Log.e(TAG, "ConnectThread: Could not create InsecureRFcomSocket " + e.getMessage());
            }
            mSocket = tap;

            mBluetoothAdapter.cancelDiscovery();

            try {
                mSocket.connect();

                Log.d(TAG, "ConnectThread: connected. ");
            } catch (IOException e) {
                try {
                    mSocket.close();
                    Log.d(TAG, "run: Closed Socket. ");
                } catch (IOException e1) {
                    Log.e(TAG, "mConnectThread: run: Unable to close connection in socket . " + e1.getMessage());
                }
                Log.d(TAG, "ConnectThread: Could not connect to UUID:" + MY_UUID_INSECURE);
            }

            connected(mSocket,mDevice);
        }
        public void cancel(){
            try{
                Log.d(TAG, "cancel: Closing Client Socket.");
                mSocket.close();
            }catch (IOException e){
                Log.e(TAG, "cancel: close() of mSocket in ConnectThread failed. " + e.getMessage());

            }
        }

    }

    public synchronized void start(){
        Log.d(TAG, "start");

        if (mConnectThread != null){
            mConnectThread.cancel();
            mConnectThread = null;
        }
        if (mInsecureAccpetThread == null){
            mInsecureAccpetThread = new AcceptThread();
            mInsecureAccpetThread.start();
        }

    }



    public void startClient(BluetoothDevice device, UUID uuid){
        Log.d(TAG,"startClient: Started.");

        mProgressDialog = ProgressDialog.show(mContext, "Connecting Bluetooth", "Please wait...", true);
        mConnectThread = new ConnectThread(device, uuid);
        mConnectThread.start();
    }

    private class ConnectedThread extends Thread{
        private final BluetoothSocket mSocket;
        private final InputStream mInStream;
        private final OutputStream mOutStream;

        public ConnectedThread(BluetoothSocket socket){
            Log.d(TAG, "ConnectedThread: Starting.");

            mSocket = socket;
            InputStream tapIn = null;
            OutputStream tanOut = null;

            try{
                mProgressDialog.dismiss();

            }catch (NullPointerException e){
                e.printStackTrace();
            }

            try {

                tapIn = mSocket.getInputStream();
                tanOut = mSocket.getOutputStream();
            }catch (IOException e){
                e.printStackTrace();
            }
            mInStream = tapIn;
            mOutStream = tanOut;
        }

        public void run(){
            byte[] buffer = new byte [1024];

            int bytes;

            while (true){
                try {
                    bytes = mInStream.read(buffer);
                    String incomingMessage = new String(buffer, 0, bytes);
                    Log.d(TAG,"InputStram: " + incomingMessage);
                }catch (IOException e){
                    Log.e(TAG, "write: Error reading inputStream. " + e.getMessage());
                    break;
                }
            }

        }

        public void write(byte[] bytes){
            String text = new String(bytes, Charset.defaultCharset());
            Log.d(TAG, "write: Writing to outputstream: " + text);
            try {
                mOutStream.write(bytes);
            } catch (IOException e) {
                Log.e(TAG, "write: Error writing to outputstream. " + e.getMessage());

            }
        }

        public void cancel () {
            try {
                mSocket.close();
            } catch (IOException e) { }
        }
    }

    private void connected(BluetoothSocket mSocket, BluetoothDevice mDevice) {
        Log.d(TAG, "connected: Starting.");

        mConnectedThread = new ConnectedThread(mSocket);
        mConnectedThread.start();
    }

    public void write(byte[] out){
        ConnectedThread r;
        Log.d(TAG, "write: write Called.");

        mConnectedThread.write(out);

    }
}
